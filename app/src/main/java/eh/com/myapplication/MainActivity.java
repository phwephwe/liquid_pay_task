package eh.com.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import eh.com.mylibrary.ButtonActivity;
import eh.com.mylibrary.WidgetBuilder;

public class MainActivity extends AppCompatActivity {

    Button btnLgoin,btnPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLgoin = (Button) findViewById(R.id.btnRegister);
        btnPayment = (Button)findViewById(R.id.btnPayment);

        String hello=ButtonActivity.hello();
        Toast.makeText(MainActivity.this,hello,Toast.LENGTH_LONG).show();

        //WidgetBuilder builder = new WidgetBuilder(MainActivity.this);
        //builder.checkAuthenticatedPerson();

        /*WidgetBuilder widgetBuilder = new WidgetBuilder(MainActivity.this);
        widgetBuilder.success(new Samplnterface() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFail() {

            }

            @Override
            public void onExit() {

            }
        });*/

        btnLgoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WidgetBuilder builder = new WidgetBuilder(MainActivity.this);
                builder.checkAuthenticatedPerson();

            }
        });

        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WidgetBuilder builder = new WidgetBuilder(MainActivity.this);
                builder.checkForPayment();
            }
        });
    }



}
