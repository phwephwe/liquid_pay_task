package eh.com.mylibrary;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Button;


public class PaymentResult extends Activity {

    Button btnSuccess,btnFail;
    public static String SUCCESS_ACTION = "eh.com.mylibrary";
    public static int success =100;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_payment_result);
        btnSuccess = (Button)findViewById(R.id.btnSuccess);
        btnFail = (Button)findViewById(R.id.btnFail);

        btnSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Constant.success = 1;
                /*Intent i = new Intent(PaymentResult.this,HomeActivity.class);
                i.putExtra("pay_status",Constant.success);
                i.putExtra("from")
                startActivity(i);*/
                Intent intent = new Intent("custom-event-name");
                // You can also include some extra data.
                intent.putExtra("success", "success");
                LocalBroadcastManager.getInstance(PaymentResult.this).sendBroadcast(intent);
                finish();

            }
        });
        btnFail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent("custom-event-name");
                // You can also include some extra data.
                intent.putExtra("success", "fail");
                LocalBroadcastManager.getInstance(PaymentResult.this).sendBroadcast(intent);
                finish();
            }
        });

    }
}
