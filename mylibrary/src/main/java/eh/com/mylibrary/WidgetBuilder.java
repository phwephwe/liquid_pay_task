package eh.com.mylibrary;

import android.content.Context;

public class WidgetBuilder {

    public Context context;
    Samplnterface samplnterface;

    public WidgetBuilder(Context context)
    {
        this.context = context;



    }




    public void checkAuthenticatedPerson()
    {
        Authenticator authenticator = new Authenticator(context);

        if(authenticator.checkLogInAlreadyOrnot())
        {

            authenticator.logInUser();
        }
        else {

            authenticator.sendRegister();
        }

    }

    public void checkForPayment()
    {
        Authenticator authenticator = new Authenticator(context);

        if(authenticator.checkLogInAlreadyOrnot())
        {

            authenticator.paymentScreen();
        }
        else {

            authenticator.sendRegister();
        }

    }

    public WidgetBuilder success(Samplnterface samplnterface)
    {
        this.samplnterface = samplnterface;
        return this;

    }

}
