package eh.com.mylibrary;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class HomeActivity extends Activity {

    EditText edtEmail,edtPassword;
    Button btnPayment,btnExit,btnSignOut;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        edtEmail = (EditText)findViewById(R.id.edtEmail);
        edtPassword = (EditText)findViewById(R.id.edtpassword);
        btnPayment = (Button)findViewById(R.id.btnPayment);
        btnExit=findViewById(R.id.btnExit);
        btnSignOut = findViewById(R.id.btnSignout);

        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(HomeActivity.this,PaymentResult.class);
                startActivity(i);



            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                System.exit(0);
                moveTaskToBack(true);



            }
        });

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getSharedPreferences("MyPref", 0);
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();
                System.exit(0);
                moveTaskToBack(true);
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();



    }


}
