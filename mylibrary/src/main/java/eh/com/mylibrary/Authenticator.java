package eh.com.mylibrary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

public class Authenticator {

    public Context context;

    public Authenticator(Context context)
    {
        this.context = context;
        LocalBroadcastManager.getInstance(context).registerReceiver(
                receiver, new IntentFilter("custom-event-name"));


    }

    public boolean checkLogInAlreadyOrnot()
    {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String email ="";
        email = pref.getString("email", ""); // getting String

        if(email.equals(""))
        {
            return false;
        }
        else {
            return true;
        }
    }

    public void sendRegister()
    {
        Intent i = new Intent(context,RegisterActivity.class);
        context.startActivity(i);
    }

    public void logInUser()
    {

        Intent i = new Intent(context,HomeActivity.class);
        context.startActivity(i);

    }

    public void paymentScreen()
    {
        Intent i = new Intent(context,PaymentResult.class);
        context.startActivity(i);


    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("success");
            Toast.makeText(context,"Payment is "+message,Toast.LENGTH_LONG).show();
            Log.e("receiver", "Got message: " + message);
        }
    };
}
